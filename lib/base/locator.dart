import 'package:get_it/get_it.dart';
import 'package:inovola_task/services/ApiService.dart';
import 'package:inovola_task/views/TripsViewModel.dart';

GetIt locator = GetIt.instance;

void setupLocator() async {
  locator.registerLazySingleton(() => ApiService());
  locator.registerFactory(() => TripsViewModel());
}
