class Strings {
  String sNoInterntConnection = "لا يوجد اتصال بالشبكة";
  String sAboutTrip = "عن الدورة";
  String sAboutTrainner = "عن المدرب";
  String sCostOfTrip = "تكلفة الدورة";
  String sConfirmReservation = "تم تأكيد حجزكم بنجاح";
  String sReserveNow = "قم بالحجز الأن";
  String sWatingOrder = "جاري تنفيذ طلبك برجاء الانتظار";
}
