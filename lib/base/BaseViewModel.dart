import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:inovola_task/base/_Assets.dart';
import 'package:inovola_task/base/_Colors.dart';
import 'package:inovola_task/base/_Strings.dart';
import 'package:inovola_task/base/locator.dart';
import 'package:inovola_task/services/ApiService.dart';

class BaseViewModel extends ChangeNotifier with Assets, CustomColors, Strings {
  final api = locator<ApiService>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  Widget buildCircleProgress(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorView(String errorText) {
    return Center(
      child: Text(
        errorText,
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
          fontFamily: "cairo",
          fontSize: 20,
        ),
      ),
    );
  }

  double screenHeight(BuildContext context, {double multiblyBy = 1}) {
    return MediaQuery.of(context).size.height * multiblyBy;
  }

  double screenWidth(BuildContext context, {double dividedBy = 1}) {
    return MediaQuery.of(context).size.width / dividedBy;
  }
}
