import 'package:flutter/material.dart';
import 'package:inovola_task/base/locator.dart';
import 'package:inovola_task/base/router.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  runApp(MyApp(),);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'inovola task',
      onGenerateRoute: Router.generateRoute,
    );
  }
}
