import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:inovola_task/base/BaseViewModel.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';

class TripsViewModel extends BaseViewModel {
  Animation delayedAnimation;
  AnimationController animationController;

  onInItAnimation() async {
    animationController =  AnimationController(vsync: AnimatedListState(), duration: Duration(seconds: 3));
    delayedAnimation = Tween(begin: 1.0, end: 0.0).animate(
      CurvedAnimation(
        parent: animationController,
        curve: Curves.easeIn,
      ),
    );
    animationController.forward();
  }

  // to change icon of fav
  bool isFavourit = false;

  onChangeFavourit() {
    isFavourit = !isFavourit;
    notifyListeners();
  }

  //getTrips
  var isLoadingTrips = false;
  var isErrorTrips = false;

  // returned data from api
  var xTripsData;

  // new list for image to remove (S) from Http
  List newImagesList = [];

  // new String for image to remove (S) from Http
  String newTrainnerImage = "";

  // total date after edit it
  String totalDate = "";

  getTrips() async {
    isLoadingTrips = true;
    notifyListeners();
    try {
      Response response = await api.onTrips();
      var model = json.decode(response.data);
      xTripsData = model;

      // remove s process
      newImagesList = [];
      List allImages = model["img"];
      allImages.forEach((soloImage) {
        String newSoloImage = soloImage;
        String soloImageAfterSplit = newSoloImage.replaceAll("https", "http");
        newImagesList.add(soloImageAfterSplit);
      });
      String traninerImage = xTripsData["trainerImg"];
      newTrainnerImage = traninerImage.replaceAll("https", "http");
      String date = xTripsData["date"];
      var splitFulDate = date.split("T");
      var currentdate = splitFulDate[0];
      var splitDate = currentdate.split("-");
      String newDayNumber = splitDate[2];
      var splitTime = splitFulDate[1].split(":");
      String newTotalTime = splitTime[0] + ":" + splitTime[1];
      var convertDate = DateTime.parse(currentdate);
      String newDayName = DateFormat('EEEE').format(convertDate);
      String newMonthName = DateFormat('MMMM').format(convertDate);

      totalDate = newTotalTime +
          " " +
          newMonthName +
          " " +
          newDayNumber +
          " " +
          newDayName;

      isLoadingTrips = false;
      isErrorTrips = false;
      notifyListeners();
    } on DioError catch (e) {
      isErrorTrips = true;
      isLoadingTrips = false;
      print("service getTrips error : ${e.response}");
      notifyListeners();
    }
  }

  //Swiper
  int swiper = 0;
  SwiperController swiperController = SwiperController();

  onChangeIndexCounter(int number) {
    swiper = number;
    notifyListeners();
  }

  //Share
  onShare(String msg) {
    Share.share(msg);
  }
}
