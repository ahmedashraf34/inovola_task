import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:inovola_task/base/BaseView.dart';
import 'package:inovola_task/views/TripsViewModel.dart';

class TripsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<TripsViewModel>(
      onModelReady: (model) {
        model.getTrips();
        model.onInItAnimation();
      },
      onModelDispose: (model) {
        model.swiperController.dispose();
        model.animationController.dispose();
      },
      builder: (xContext, model, child) => Scaffold(
        key: model.scaffoldKey,
        body: model.isLoadingTrips
            ? model.buildCircleProgress(context)
            : model.isErrorTrips
                ? model.buildErrorView(model.sNoInterntConnection)
                : Container(
                    color: Colors.white,
                    width: model.screenWidth(context),
                    child: ListView(
                      children: <Widget>[
                        build_images_slider(model, context),
                        build_Text_item(
                          xFontText: "# " + model.xTripsData["interest"],
                          xFontSize: model.screenWidth(context) > 600 ? 25 : 15,
                          xFontColor: model.cGrey1,
                          model: model,
                          context: context,
                        ),
                        build_Text_item(
                          xFontText: model.xTripsData["title"],
                          xFontColor: Colors.grey[500],
                          xFontSize: model.screenWidth(context) > 600 ? 35 : 25,
                          xFontSpacing: 1,
                          xFontWeight: FontWeight.w800,
                          model: model,
                          context: context,
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 5),
                            child: Row(
                              textDirection: TextDirection.rtl,
                              children: <Widget>[
                                Image.asset(
                                  model.aCalendar,
                                  height: model.screenWidth(context) > 600
                                      ? 30
                                      : 20,
                                  color: model.cGrey1,
                                ),
                                build_Text_item(
                                  xFontText: model.totalDate,
                                  xFontSize: model.screenWidth(context) > 600
                                      ? 25
                                      : 15,
                                  xFontColor: model.cGrey1,
                                  model: model,
                                  context: context,
                                ),
                              ],
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 5),
                            child: Row(
                              textDirection: TextDirection.rtl,
                              children: <Widget>[
                                Image.asset(
                                  model.aPin,
                                  height: model.screenWidth(context) > 600
                                      ? 30
                                      : 20,
                                  color: model.cGrey1,
                                ),
                                build_Text_item(
                                  xFontSize: model.screenWidth(context) > 600
                                      ? 25
                                      : 15,
                                  xFontText: model.xTripsData["address"],
                                  xFontColor: model.cGrey1,
                                  model: model,
                                  context: context,
                                ),
                              ],
                            ),
                          ),
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 2,
                        ),
                        build_Text_item(
                          xFontText: model.sAboutTrainner,
                          xFontColor: model.cGrey1,
                          model: model,
                          xFontWeight: FontWeight.bold,
                          xFontSize: model.screenWidth(context) > 600 ? 25 : 15,
                          context: context,
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 5),
                            child: Row(
                              textDirection: TextDirection.rtl,
                              children: <Widget>[
                                CircleAvatar(
                                  radius: model.screenWidth(context) > 600
                                      ? 30
                                      : 20,
                                  backgroundImage: CachedNetworkImageProvider(
                                    model.newTrainnerImage,
                                    errorListener: () => Icon(Icons.error),
                                  ),
                                ),
                                build_Text_item(
                                  xFontText: model.xTripsData["trainerName"],
                                  xFontColor: model.cGrey1,
                                  xFontWeight: FontWeight.bold,
                                  xFontSize: model.screenWidth(context) > 600
                                      ? 25
                                      : 15,
                                  model: model,
                                  context: context,
                                ),
                              ],
                            ),
                          ),
                        ),
                        build_Text_item(
                          xFontText: model.xTripsData["trainerInfo"],
                          xFontColor: model.cGrey1,
                          model: model,
                          xFontSize: model.screenWidth(context) > 600 ? 25 : 15,
                          context: context,
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 2,
                        ),
                        build_Text_item(
                          xFontText: model.sAboutTrip,
                          xFontColor: model.cGrey1,
                          xFontWeight: FontWeight.bold,
                          model: model,
                          xFontSize: model.screenWidth(context) > 600 ? 25 : 15,
                          context: context,
                        ),
                        build_Text_item(
                          xFontText: model.xTripsData["occasionDetail"],
                          xFontColor: model.cGrey1,
                          model: model,
                          xFontSize: model.screenWidth(context) > 600 ? 25 : 15,
                          context: context,
                        ),
                        Divider(
                          color: Colors.grey[300],
                          thickness: 2,
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 5),
                            child: Row(
                              children: <Widget>[
                                build_Text_item(
                                  xFontText: "SAR " +
                                      model.xTripsData["price"].toString(),
                                  xFontColor: model.cGrey1,
                                  xFontWeight: FontWeight.bold,
                                  model: model,
                                  xFontSize: model.screenWidth(context) > 600
                                      ? 25
                                      : 15,
                                  context: context,
                                ),
                                Spacer(),
                                build_Text_item(
                                  xFontText: model.sCostOfTrip,
                                  xFontWeight: FontWeight.bold,
                                  xFontColor: model.cGrey1,
                                  model: model,
                                  xFontSize: model.screenWidth(context) > 600
                                      ? 25
                                      : 15,
                                  context: context,
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        GestureDetector(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                Future.delayed(Duration(seconds: 2), () {
                                  Navigator.pop(context);
                                  model.scaffoldKey.currentState.showSnackBar(
                                    SnackBar(
                                      backgroundColor: model.cPurp1e,
                                      duration: Duration(seconds: 2),
                                      content: Text(
                                        model.sConfirmReservation,
                                        style: TextStyle(
                                          fontFamily: model.fontfamily,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w900,
                                          fontSize:
                                              model.screenWidth(context) > 600
                                                  ? 27
                                                  : 17,
                                        ),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  );
                                });
                                return buildSimpleDialog(context, model);
                              },
                            );
                          },
                          child: Container(
                            alignment: Alignment.center,
                            height: 60,
                            color: model.cPurp1e,
                            width: model.screenWidth(context),
                            child: Text(
                              model.sReserveNow,
                              style: TextStyle(
                                fontFamily: model.fontfamily,
                                color: Colors.white,
                                fontWeight: FontWeight.w900,
                                fontSize:
                                    model.screenWidth(context) > 600 ? 27 : 17,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
      ),
    );
  }

  SimpleDialog buildSimpleDialog(BuildContext context, TripsViewModel model) {
    return SimpleDialog(
      backgroundColor: model.cPurp1e,
      contentPadding: EdgeInsets.all(5),
      shape: ContinuousRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15))),
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              GestureDetector(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                    ),
                    child: Icon(
                      Icons.close,
                      color: Colors.white,
                      size: model.screenWidth(context) > 600 ? 35 : 25,
                    ),
                  ),
                ),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Text(
                  model.sWatingOrder,
                  style: TextStyle(
                    fontSize: model.screenWidth(context) > 600 ? 30 : 20,
                    fontFamily: model.fontfamily,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                  textDirection: TextDirection.rtl,
                  textAlign: TextAlign.right,
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Icon(
                  Icons.watch_later,
                  color: Colors.white,
                  size: model.screenWidth(context) > 600 ? 35 : 25,
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget build_Text_item({
    @required String xFontText,
    FontWeight xFontWeight = FontWeight.w500,
    double xFontSpacing = 0,
    double xFontSize = 15.0,
    Color xFontColor,
    TripsViewModel model,
    BuildContext context,
  }) {
    return AnimatedBuilder(
      animation: model.animationController,
      builder: (ctx, child) {
        return Transform(
          transform: Matrix4.translationValues(
            model.delayedAnimation.value * MediaQuery.of(context).size.width,
            0.0,
            0.0,
          ),
          child: Container(
            child: Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: Text(
                  "${xFontText}",
                  textAlign: TextAlign.right,
                  textDirection: TextDirection.rtl,
                  style: TextStyle(
                    fontFamily: model.fontfamily,
                    color: xFontColor,
                    fontWeight: xFontWeight,
                    letterSpacing: xFontSpacing,
                    fontSize: xFontSize,
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Stack build_images_slider(TripsViewModel model, BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: model.screenHeight(context, multiblyBy: 0.40),
          child: Swiper(
            itemCount: model.newImagesList.length,
            index: model.swiper,
            autoplay: true,
            onIndexChanged: model.onChangeIndexCounter,
            itemBuilder: (BuildContext context, int index) {
              return CachedNetworkImage(
                imageUrl: model.newImagesList[index],
                placeholder: (context, url) =>
                    model.buildCircleProgress(context),
                errorWidget: (context, url, error) => Icon(
                  Icons.error,
                ),
                fit: BoxFit.fill,
              );
            },
            pagination: SwiperPagination(
              alignment: Alignment.bottomLeft,
              builder: DotSwiperPaginationBuilder(
                activeColor: Colors.white,
                color: Colors.grey[300],
                size: model.screenWidth(context) > 600 ? 15 : 8,
                activeSize: model.screenWidth(context) > 600 ? 19 : 12,
              ),
            ),
            controller: model.swiperController,
          ),
        ),
        Positioned(
          top: 10,
          left: 10,
          child: Row(
            children: <Widget>[
              GestureDetector(
                child: Icon(
                  model.isFavourit ? Icons.star : Icons.star_border,
                  color: Colors.white,
                  size: model.screenWidth(context) > 600 ? 40 : 30,
                ),
                onTap: () {
                  model.onChangeFavourit();
                },
              ),
              GestureDetector(
                onTap: () {
                  model.onShare("app link");
                },
                child: Icon(
                  Icons.share,
                  color: Colors.white,
                  size: model.screenWidth(context) > 600 ? 40 : 30,
                ),
              ),
            ],
          ),
        ),
        Positioned(
          top: 10,
          right: 10,
          child: GestureDetector(
            onTap: (){
              SystemNavigator.pop();
            },
            child: Icon(
              Icons.keyboard_arrow_right,
              color: Colors.white,
              size: model.screenWidth(context) > 600 ? 45 : 35,
            ),
          ),
        ),
      ],
    );
  }
}
