import 'package:dio/dio.dart';

class ApiService {
  Dio _dio;

  ApiService() {
    _dio = Dio()
      ..interceptors.add(
        LogInterceptor(
          responseBody: true,
          error: true,
          requestBody: true,
          requestHeader: true,
        ),
      );
  }

  //onTrips
  Future<Response> onTrips() {
    return _dio.get(
      "http://skillzycp.com/api/UserApi/getOneOccasion/389/0",
    );
  }
}
